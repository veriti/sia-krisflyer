'use strict';

import $ from 'jquery';

export default function () {
  $('.grid').masonry({
    itemSelector: '.grid__item',
    // columnWidth: '.grid__sizer',
    // gutter: '.grid__gutter',
    // percentPosition: true,
    fitWidth: true,
    gutter: 20,
  });
}
