'use strict';

// shim layer with setTimeout fallback
window.requestAnimFrame = (function () {

  return window.requestAnimationFrame  ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function (callback) {
      window.setTimeout(callback, 1000 / 60);
    };

})();

/**
 * By Ken Fyrstenberg Nilsen
 *
 * drawImageProp(context, image [, x, y, width, height [,offsetX, offsetY]])
 *
 * If image and context are only arguments rectangle will equal canvas
*/


(function (win, $) {

  // TODO: refactor as a jQuery plugin

  var tmpCanvas = $('<canvas/>').get(0);
  var $parallax = $('.parallax');

  function init() {
    registerBindings();
  }

  function drawImageProp(ctx, img, x, y, w, h, offsetX, offsetY) {

    if (arguments.length === 2) {
      x = y = 0;
      w = ctx.canvas.width;
      h = ctx.canvas.height;
    }

    // default offset is center
    offsetX = typeof offsetX === "number" ? offsetX : 0.5;
    offsetY = typeof offsetY === "number" ? offsetY : 0.5;

    // keep bounds [0.0, 1.0]
    if (offsetX < 0) offsetX = 0;
    if (offsetY < 0) offsetY = 0;
    if (offsetX > 1) offsetX = 1;
    if (offsetY > 1) offsetY = 1;

    var iw = img.width,
    ih = img.height,
    r = Math.min(w / iw, h / ih),
        nw = iw * r,   // new prop. width
        nh = ih * r,   // new prop. height
        cx, cy, cw, ch, ar = 1;

    // decide which gap to fill
    if (nw < w) ar = w / nw;
    if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;  // updated
    nw *= ar;
    nh *= ar;

    // calc source rectangle
    cw = iw / (nw / w);
    ch = ih / (nh / h);

    cx = (iw - cw) * offsetX;
    cy = (ih - ch) * offsetY;

    // make sure source rectangle is valid
    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    // fill image in dest. rectangle
    ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);
  }

  function registerBindings() {
    $parallax.each(parallax);

    $(win).on('load', function () { $parallax.trigger('resize.parallax'); });

    $(win).on('resize', function () { $parallax.trigger('resize.parallax'); });

    // $(win).on('scroll', function () { $parallax.trigger('scroll.parallax'); });
  }

  function parallax(index) {

    var $this = $(this);
    var $bg = $this.find('.parallax__bg');
    var canvas = $('<canvas/>').get(0);
    var context = canvas.getContext('2d');
    var ticking = false;
    var lastScrollY = 0;
    var image = new Image();
    var tmpContext = null;

    $bg.html(canvas);
    image.src = $bg.data('image');

    function redraw() {

      var posY = (lastScrollY / 5) * -1;
      var clipX = (canvas.width / 2) - 20;
      var per = image.width / image.height;

      var drawParams = [
        image,           // the image element
        0,               // x of subrectangle of source image
        0,               // y of subrectangle of source image
        image.width,     // width of the source' subrectangle
        image.height,    // height of the source' subrectangle
        // 0,               // x of canvas
        // 0,               // y of canvas
        // canvas.width * per,     // width of the canvas crop
        // canvas.height,    // height of the canvas crop
      ];

      if (index)  {
        context.drawImage(tmpContext.canvas, clipX, 0);
        context.globalCompositeOperation = 'source-out';
      }

      drawImageProp(context, image, 0, 0, canvas.width, canvas.height, 0.5, 0);
      ticking = false;
    }

    function cacheClipper() {
      tmpContext = tmpCanvas.getContext('2d');
      tmpContext.fillStyle = '#112038';
      tmpContext.beginPath();
      tmpContext.moveTo(0, 0);
      tmpContext.lineTo(40, 0);
      tmpContext.lineTo(20, 20);
      tmpContext.closePath();
      tmpContext.fill();
    }

    cacheClipper();

    function onResize() {
      canvas.width = $bg.outerWidth();
      canvas.height = $bg.outerHeight();
      redraw();
    }

    function onScroll() {
      if (!ticking) {
        ticking = true;
        win.requestAnimFrame(redraw);
        lastScrollY = win.pageYOffset;
      }
    }

    $this.on('resize.parallax', onResize);
    $this.on('scroll.parallax', onScroll);

    return $this;
  }

  init();

})(window, jQuery);
