'use strict';

import window from 'window';
import WebFont from 'webfontloader';

WebFont.load({
  google: { families: ['Lato:400,300,100,700,900'] },
  timeout: 2000,
  fontinactive: function () { window.console.log('cannot load fonts!'); }
});
