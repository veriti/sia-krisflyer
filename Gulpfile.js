'use strict';

var path = require('path');
var through = require('through2');
var gulp = require('gulp');
var gutil = require('gulp-util');

var uglify = require('gulp-uglify');

var rollup = require('rollup').rollup;
var rollupCommonJS = require('rollup-plugin-commonjs');
var rollupNodeResolve = require('rollup-plugin-node-resolve');
var rollupUglify = require('rollup-plugin-uglify');

var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

var del = require('del');

var browserSync = require('browser-sync');

var sassLib = [
  'node_modules/normalize.css',
  'node_modules/bootstrap-sass/assets/stylesheets',
  'node_modules/animatewithsass',
];

gulp.task('styles', function () {
  return gulp.src('source/scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ includePaths: sassLib }).on('error', sass.logError))
    .pipe(postcss([autoprefixer({ browsers: ['last 2 version'] })]))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('public/assets/styles'))
    .pipe(browserSync.stream());
});

gulp.task('styles:dist', function () {
  return gulp.src('source/scss/*.scss')
    .pipe(sass({ includePaths: sassLib, outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(postcss([autoprefixer({ browsers: ['last 2 version'] })]))
    .pipe(gulp.dest('public/assets/styles'));
});

function buildModernizr(configPath) {

  var Modernizr = require('modernizr'),
      config = require(path.resolve(configPath)),
      stream = through.obj(function (file, enc, cb) {
        this.push(file);
        cb();
      });

  Modernizr.build(config, function (result) {

    var file = new gutil.File({
      path: 'modernizr.min.js',
      contents: new Buffer(result),
    });

    stream.write(file);
    stream.end();
    stream.emit('end');

  });

  return stream;

}

gulp.task('scripts:modernizr', function () {
  return buildModernizr('source/scripts/modernizr-config.js')
    .pipe(uglify())
    .pipe(gulp.dest('public/assets/scripts'));
});

gulp.task('scripts:main', function () {
  return rollup({
    entry: 'source/scripts/main.js',
    plugins: [
      rollupUglify(),
    ],
  }).then(function (bundle) {
    return bundle.write({
      globals: {
        window: 'window',
        modernizr: 'Modernizr',
        jquery: 'jQuery',
      },
      format: 'iife',
      dest: 'public/assets/scripts/main.js',
    });
  });
});

gulp.task('scripts:webfonts', function () {
  return rollup({
    entry: 'source/scripts/webfontloader.js',
    plugins: [
      rollupNodeResolve(),
      rollupCommonJS({
        include: 'node_modules/**',
      }),
      rollupUglify(),
    ],
  }).then(function (bundle) {
    return bundle.write({
      format: 'iife',
      dest: 'public/assets/scripts/webfontloader.js',
    });
  });
});

var jsLib = [
  // 'node_modules/waypoints/lib/jquery.waypoints.min.js',
  'node_modules/jquery/dist/jquery.min.js',
  // 'node_modules/masonry-layout/dist/masonry.pkgd.min.js',
];

gulp.task('scripts:libraries', function () {
  return gulp.src(jsLib)
    .pipe(uglify())
    .pipe(gulp.dest('public/assets/scripts'));
});

gulp.task('scripts', ['scripts:modernizr', 'scripts:libraries', 'scripts:main']);

gulp.task('clean', function () {
  del.sync([
    'public/assets/styles/*',
    'public/assets/scripts/*',
  ]);
});

gulp.task('serve', ['scripts', 'styles'], function () {
  browserSync.init({
    server: './public',
    open: false,
  });
  gulp.watch('source/**/*.scss', ['styles']);
  gulp.watch('source/**/*.js', ['scripts']);
  gulp.watch('public/**/*.html', browserSync.reload);
});

gulp.task('build', ['clean', 'scripts', 'styles:dist']);
