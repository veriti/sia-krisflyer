# KrisFlyer Microsite

## Development

#### Requirements

- Node 4+
- Install project dependencies

```
npm install
```

#### Development Server

```
gulp serve
```

## Building

```
gulp build
```


# TODO

- [ ] add class invoke with modernizr
- [ ] remove all mobile references for now
- [ ] set parallax triggerpoints with waypoint
